This is the jetring package from the salsa.debian.org, with our minor modifications
to become compilable under cygany, which is a debian overlay over cygwin.

Our cygany branch tracks master in the salsa repository, roughly so:

Git remotes:
```
origin   git@gitlab.com:cygany/jetring.git (fetch)
origin   git@gitlab.com:cygany/jetring.git (push)
upstream https://salsa.debian.org/debian/jetring.git (fetch)
upstream https://salsa.debian.org/debian/jetring.git (push)
```

Branch setup:
```
* cygany  c0deebe [origin/cygany] ...
  debian  c0deebe [upstream/debian] ...
```

In our current state, it can be only compiled roughly so:

```
PERL5LIB=/usr/share/perl5 DEB_RULES_REQUIRES_ROOT=no debian/rules build binary
```

But soon it will be hopefully better.
